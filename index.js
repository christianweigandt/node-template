const express = require('express');
const app = express();


app.use(express.static('public'));
app.use(express.urlencoded({
	extended: false
}));
app.use(express.json());
app.set('view engine', 'pug');
app.set('views', './views');
app.listen(8080);


require('./local_modules/routes') (app);

/* ------------------------ 404 ----------------------- */
app.get('*', function(req, res) {
    res.status(404);
    // res.render('notfound', {name: '404 Not Found'});
});